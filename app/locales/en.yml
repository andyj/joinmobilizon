meta:
  title: '@:home.title ! #JoinMobilizon'
  description: '@:home.intro.title'
menu:
  faq: F.A.Q.
  help: Help
  docs: Documentation
  code: Source code
  instances: Instances
  hall-of-fame: Hall of fame
  news: News
link:
  forumPT: https://framacolibri.org/c/qualite/mobilizon
home:
  title: Let’s take back control of our events
  subtitle: Together, we go further
  intro:
    tags:
    - Gather
    - Organize
    - Mobilize
    title: A user-friendly, emancipatory and ethical tool for gathering and organizing,
      to mobilize.
    support: Support us
  intro2:
    title: A free and federated tool to get our events off Facebook!
  what:
    text1: Mobilizon is a tool designed to create platforms for <b>managing communities
      and events</b>. Its aim is to help as many people as possible avoid using
      Facebook groups and events, Meetup, etc.
    text2: Mobilizon is offered under a free license, meaning anyone can host a 
      Mobilizon server, called an <b>instance</b>. These instances can optionally 
      <b>federate</b>, so that a person with an account on <i>ExampleMeet</i> can
      register for an event posted on <i>SpecimenEvent</i>.
  project:
    p:
    - From friends-and-family birthday parties to marches against climate change,
      our gatherings are trapped in the tech giants' platforms. How can we get 
      organized, or click “Going” on an event, without providing intimate data 
      to Facebook or getting locked in to MeetUp?
    - 'At Framasoft, we want to produce <b>Mobilizon</b>: a piece of free-libre 
      software that will allow communities to create their own spaces to publish
      events, without the intrusion of the tech giants. We want to develop a <b>digital
      common</b>, that everyone can make their own, with <b>respect for privacy 
      and activism</b> by design.'
    - We had from help designers to consider what Mobilizon could be. We took time to study
      the digital habits of activists in order to understand the features they need
      to gather, organize, and mobilize.
    - We want to make Mobilizon a user-friendly, emancipatory and ethical tool. Whether
      you want to create your event in a Mobilizon instance, or install
      your own Mobilizon site… the functionality of this software will depend on
      the means you give us to develop it.
    legend:
    - Layout of an event page – G. Dorne and M.-C. Paccard
    - Layout of a group page – G. Dorne and M.-C. Paccard
  steps:
    title: A beta version in the fall of 2019
    intro: 'This fundraising will determine what means we will have to work on the
      software, in order to release a beta version in the fall of 2019</b>. We have
      60 days to make this fundraising known and learn which Mobilizon you want to
      make happen. Here are the (cumulative) steps that your money can help to finance:'
    minimaliste:
      title: Step n°1
      subtitle: Free & basic
      ul:
      - Events publishing tool
      - UX studies
      - Graphic design
      - Free & documented code
      p:
      - This sum will cover our prototype expenses and help us complete it.
      - We will recover the funds already spent designing and promoting the software,
        developing its basic functionality and paying the UX and UI designers who
        enabled its design.
      - The code will be returned to the community so that anyone can take over and
        maintain it.
    federe:
      title: Step n°2
      subtitle: Emancipating & federated
      ul:
      - Federation and links
      - Administration tools
      - Test/demo instance
      - ActivityPub integration
      p:
      - 'We will be able to implement a protocol essential to Mobilizon’s success:
        ActivityPub federation. Federation means that any community will be
        able not only to install Mobilizon on their own servers, but also to connect
        and sync with others.'
      - This will help the number of entry points to grow, decentralize
        the data, and connect to a collection of federated sites that already 
        includes Mastodon (a federated alternative to Twitter) and PeerTube 
        (a federated alternative to YouTube).
      - We will also open a demo instance of this beta version, so that everyone can
        see for themselves what the software will feel like.
    ideal:
      title: Step n°3
      subtitle: Ideal & friendly
      ul:
      - Group management
      - Messaging
      - Multi-identities
      - Managing external tools
      p:
      - We can make the Mobilizon of our dreams come true!
      - People will be able to create and manage groups in Mobilizon, with messaging
        tools to facilitate exchange between members. Mobilizon will also display
        a feature to manage the external tools that the group already uses to write
        documents together, choose the date of the next meeting, etc.
      - Finally, each person with an account will be able to create several identities,
        in order, for example, not to display the same identity on their sporting events,
        family reunions and activist marches.
    version-1:
      title: and beyond…?
      subtitle: Durable & resilient
      ul:
      - Funded beyond V1
      - Advanced Mapping
      - Evolution of interfaces
      - Mobile application
      p:
      - 'We have done our calculations: €50,000 will be enough to make 
        the best software we can make. If we receive more money, we won’t do more,
        or go faster, but we can go further.'
      - In the fall of 2019, we will release a beta version. From the feedback and
        comments we will then receive, we will work on a complete first version, a
        V1, which we plan for the first half of 2020.
      - But even after the V1 release, there will still be a lot of work left! For
        example, we wish we'll someday be able to set up a mapping server facilitating
        events’ localization, and create a Mobilizon app for smartphones, and many
        other things…
    understand: Learn more about those steps
  allocation:
    title: Where will your money go?
    if: If we get
    intro:
    - We have organized this fundraiser on a self-hosted website in order to reduce bank
      and crowdfunding fees. Our goal is to promote a new model to fund digital commons,
      based on <a href="https://joinpeertube.org/en/news">our experience with PeerTube</a>.
    - Aware of the issues raised by the centralization of Facebook events, we used
      our own finances to design and prototype an alternative tool, Mobilizon. We
      now want to know to what extent you will support this project. The more you
      give, the more involved we can be in the development and the maintenance of
      Mobilizon.
    expenses:
    - Bank and financial fees
    - Conception and design
    - Development
    - Project coordination
    - Communication
    steps:
    - We funded the prototyping and conception of Mobilizon, hiring designers and
      writing the basic code. Reaching this first step will help us refund what we've 
      already spent and finalize the code, so that we can deliver it to the community.
    - Funding during the 2<sup>nd</sup> step will allow the integration
      of the ActivityPub federation protocol, as well as the development of 
      administration tools for future hosts of instances.
    - 'The 3<sup>rd</sup> step will enable us to design and develop features
      that Facebook and their peers would never dare to offer: group self-management,
      messaging, multiple identities and external tool display and management.'
  donation:
    title: Mobilizon is supported by Framasoft, a French not-for-profit company.
    texts:
    - '@:data.html.soft (it’s us!) is a non-profit association created in 2004, which
      now focuses on <b>popular education</b> on the stakes of our digital world.
      Our small organisation (less than 40 members, less than 10 employees) is known
      for carrying out the project <a href="@:data.link.dio">Degoogle-ify the Internet</a>,
      offering 34 ethical online alternatives to Google's tools.'
    - Since 2017, we have been developing <a href="https://joinpeertube.org/">PeerTube</a>
      a free and federated alternative to YouTube. Thanks to your donations from a
      previous crowdfunding, PeerTube is now enjoying increasing success. Following 
      this experience, we conceived the Mobilizon project.
    - Recognized as serving a general interest, our organisation is more than 90% funded
      through <b>donations</b>, mainly from our French supporters. Today, we need
      your generosity to help us make Mobilizon a reality, worldwide.
  team:
    title: About the team
    more: More about @:data.color.soft
    guests:
      title: Guest Designers
      intro: We asked two designers to come and work with us on this project, so that
        Mobilizon would correspond right from the outset to the needs of the
        people who will use it.
    members:
      mcgp:
        desc: “Independent designer, UX researcher to understand the uses in depth
          and design usable, ethical and inclusive tools.”
      dorne:
        desc: “Independent designer and founder of Design & Human”
  funding:
    goal: with a goal of
    donators: contributors
    days: days
    hours: hours
    deadline: before the end of the campaign
    step-on: (step {step} of {max})
    share: Share and follow
    news: Follow Mobilizon's news
    news-alt: Newsletter
    who: Project leader
    asso: The not-for-profit @:data.html.soft
    where: Lyon, France
    when-from: Campaign from
    when-to: to
    more: more info
    anonymous: Anonymous
    rewards:
      title: My contribution
      d1:
        title: 1 € or more
        subtitle: <small>equivalent to </small> a hug to the team!
        p: 'You show us your trust, your support, and you participate in the birth
          of Mobilizon: THANK YOU!'
      d25:
        title: 25 € or more
        subtitle: <small>equivalent to</small> 15 minutes of chat
        p: Meetings, phone calls and interviews… Sometimes, creating a user-friendly
          software requires more saliva than sweat!
      d42:
        title: 42 € or more
        subtitle: <small>equivalent to</small> one hour of code
        p: If we include payroll expenses, offices, equipment… it’s the price of
          code written under humane conditions.
      d88:
        title: 88 € or more
        subtitle: <small>equivalent to</small> a brainstorm
        p: Getting together around a note board… that’s how the most beautiful ideas
          are born!
      d130:
        title: 130 € or more
        subtitle: <small>equivalent to</small> an UX interview
        p: When you want to design software made for humans, your best move
          is to ask them questions.
      donators: Contributors
      free: Contribute a different amount...
    sponsors:
      title: Sponsors’ corner
      intro: For organizations that want to show their support for the project, for
        the entire duration of V1, subject to acceptance by the association.
      d1000:
        title: 1000 € or more
        p: Your logo will be highlighted in the joinmobilizon.org hall of fame
      d2500:
        title: 2500 € or more
        p: Your logo will also be, by default, in the “about” page of the software
  quote:
    text: We won’t change the world from Facebook. The tool we dream of, surveillance
      capitalism corporations won’t develop it, as they couldn’t profit from it. <br>
      This is an opportunity to build something better, by taking another approach.
    blog:
      text: Read the Framasoft intention note on the Framablog (in English)
      link: https://framablog.org/2019/05/14/mobilizon-lets-finance-a-software-to-free-our-events-from-facebook/
  how-it-works:
    how:
      text:
        convivial:
          icon: group
          title: Convivial and practical
          text: Mobilizon doesn’t try to lock you in its platform to manage your community
            nor to direct your ways.<br> Its goal is to help you integrate the 
            collaborative tools of your choice, leaving you free
            to organize your community and gatherings your own way.
        emancipation:
          icon: address-card
          title: Emancipating and respectful
          text: Mobilizon gives you the ability to engage without revealing yourself,
            to organize without exposing yourself. For instance, with just one account,
            you will get several identities, used as social masks.
        ethic:
          icon: smile-o
          title: Ethical and decentralized
          text: The free licence of Mobilizon software is a guarantee of its transparency,
            its contributive aspect and the openness of its governance. Having several
            Mobilizon instances will let you choose where you want to create your
            account, so you can decide to who you trust your data.
  timeline:
    title: And next… the roadmap
    image-alt: Illustration of Mobilizon in Contributopia
    events:
      init:
        date: October 2018
        text: Announcement of Framasoft’s intentions for Mobilizon
      study:
        date: End 2018 - Beginning 2019
        text: UX Studies, Usage-centered design and prototyping
      crowdfunding:
        date: May 2019
        text: <strong>Crowdfunding determining the means to develop Mobilizon</strong>
      beta:
        date: Fall 2019
        text: Release of the beta version according to the funds obtained
      rc:
        date: End of 2019
        text: Improvements based on first use in beta
      v1:
        date: 1st half of 2020
        text: Release of a v1…?
  contribute:
    title: We want to create Mobilizon with you so that we can make it the tool
           we know it can be
    text: Want to get involved in this project, but don’t know where to begin?
    ideas:
      support:
        title: Support Framasoft
        text: Support Framasoft financially, who has invested in this project
        link: https://soutenir.framasoft.org/
        icon: euro
      talk_about:
        title: Spread the word about Mobilizon
        text: If you know people who may be interested in the Mobilizon project, talk
          to them about it!
        icon: share
      feedback:
        title: Share your feedback
        text: Are you happy to use Mobilizon? Tell us about it!
        link: https://framacolibri.org/c/mobilizon
        icon: how to do it
      discuss:
        title: Join the discussion
        text: Help us shape the future of Funkwhale
        link: https://framacolibri.org/c/mobilizon
        icon: how to do it
      translate:
        title: Translate Mobilizon
        text: Do you know a particular language? Help translate the interface of Mobilizon.
        icon: language
      ask_functionnality:
        title: Request a feature
        text: Are you missing something? Describe your dream functionality and engage
          the discussion
        link: https://framacolibri.org/c/mobilizon/feature
        icon: question
      install:
        title: Install Mobilizon
        text: Take your place in the federation and let’s exchange!
        link: https://framagit.org/framasoft/mobilizon/wikis/install
        icon: server
    utopia: Let’s equip the people making a different world a reality.
form:
  step1:
    title: Support Mobilizon
    oneshot: One-time donation
    oneshot_ex: 'ex: 42'
    other: Amount
    newsletter: I would like to be informed of Mobilizon’s progress
    anonymous: I want my donation to remain anonymous
    defisc: I would like to receive a tax receipt
  step2:
    intro: Please complete this information for the receipt edition
    private: Confidentiality
    type: I represent
    part: an individual
    corp: a company
    corp_tip: Company, association, community…
    society: Legal entity
    society_ex: e.g. Free Software Foundation
    society_email_ex: e.g. contact@fsf.org
    nickname: Nickname
    nickname_ex: e.g. rms
    lastname: Last name
    lastname_ex: e.g. Stallman
    firstname: First name
    firstname_ex: e.g. Richard
    email: E-mail
    email_ex: e.g. r.stallman@outlock.com
    address1: Address
    address1_ex: e.g. 12, freedom street
    address2: Addition Line 2
    address2_ex: e.g. Building VI
    zip: Zip code
    zip_ex: e.g. 69007
    city: City
    city_ex: e.g. Lyon
    country: Country
    error_empty: This field must be filled in.
    error_email: You must enter a valid email address.
  step3:
    title: Payment method
    defisc_text: In France, thanks to the tax deduction of {percent}, <b>your donation
      of {amount}</b> will cost you {defisc}.
    cb: Credit card
    pp: Paypal
    i_give: Donate
    now: now
contact:
  title: Contact us!
  source: Source code
  forum:
    link: https://framacolibri.org/c/mobilizon/
footer:
  credits: '© <a href="https://framasoft.org/en/">Framasoft</a> 2018 <a href="https://framagit.org/framasoft/joinmobilizon/joinmobilizon">AGPL-3.0</a>
    | '
  photo: ' <a href="https://pixabay.com/fr/action-collaborer-collaboration-2277292/">Picture</a>
    by rawpixel on Pixabay | '
  picture: ' Illustrations by <a href="https://twitter.com/ninalimpi">Katerina Limpitsouni</a>
    for <a href="https://undraw.co/">Undraw</a> and <a href="https://davidrevoy.com">David
    Revoy</a> for <a href="https://contributopia.org/">Contributopia</a>.'
merci:
  title: Thank you very much for your donation to the Mobilizon project!
  continue: Thanks to you donation, Framasoft’s teams will be able to continue and
    deepen the work already started on this software.
  refresh: 'If your donation is not anonymous, you should see your name appear in the hall of Fame page within the next 10 minutes (please refresh to check).'
  success: 'The success of this crowdfunding depends only on you: we have until July
    10 to find out how much interest there is in this project and what resources
    we will have to develop it.'
  comment: Feel free to share your beautiful approach on your networks social favorites
    and to talk about them around you…
  share: Support Mobilizon, a free and federated tool to get our events
    off Facebook!
  shareOn: Share on
  shareTitle: Let’s support Mobilizon
hof:
  title: Hall of Fame
  sponsors: Sponsors
  donators: Donors
  dev: Code Contributors
  contrib: Contribute to the code
faq:
  title: Learn more about Mobilizon…
  clic: (click on the questions to reveal the answers)
  prez:
    title: Presentation of Mobilizon
    what:
      q: What is Mobilizon?
      a:
        p:
        - It’s software which, once installed on a server, will create a website where
          people can create events, in the same way as on Facebook events or MeetUp.
        - Installing Mobilizon will allow groups and collectives to escape the
          services of tech giants by creating their own event platform. This installation
          (called an "instance") can simply interconnect with others, thanks to <a href="https://en.wikipedia.org/wiki/ActivityPub">a
          decentralized federation protocol</a>.
        - Mobilizon will be developed to meet the needs of activists, who need specific
          tools to organize themselves in an autonomous and sustainable way. 
          However, is equally useful for organising a birthday or a karate competition!
    facebook:
      q: Will Mobilizon replace Facebook?
      a:
        p:
        - Mobilizon is not conceived as "the Facebook killer".
        - We can see the danger of publishing an activist rally on Facebook, a monopolistic
          platform that <a href="https://dayssincelastfacebookscandal.com/">continuously
          generates scandals</a> involving privacy or the manipulation of public opinion.
          Compared to this tech giant, our means and aims are modest.
        - 'Our ambition is as follows: let’s start with a tool that does little
          but that does it well, and that will be a solid basis for building more.
          Let’s focus first on the specific needs of a particular audience (activists),
          which will not prevent Mobilizon from being used in other cases or
          from being fitted to the needs of other audiences.'
        - Producing Mobilizon is not a sprint, where you promise everything to everyone.
          It’s more of a cross-country race, where the first step is to develop 
          a tool for anyone to organise events (and to do it well!)
    features:
      q: Will Mobilizon allow me to...?
      a:
        p:
        - 'Make recurring events, share my events on Twitter, send private messages...
          etc. There is but one answer:'
        - It depends! :)
        - The list of features for v1 will depend on the amount of crowdfunding we receive.
        - And what happens after v1 will depend on the Mobilizon community.
        - Since the success of this crowdfunding depends more on you than on us, we can
          promise that we will keep you informed of Mobilizon’s progress along
          the way, on <a href="@:data.baseurl@:lang/news">the news page of this site</a>
          and the dedicated newsletter.
    advantages:
      q: What are the 3 main advantages of Mobilizon?
      a:
        p:
        - '<b>Free</b> : Mobilizon’s license guarantees respect for the fundamental
          freedoms of the people who will use it. Because its <a href="https://framagit.org/framasoft/mobilizon/">source
          code is published</a>, it is publicly auditable, which guarantees its
          transparency. If the direction given by the development team does not suit
          you, you have the legal right to create a version of the software with
          your own governance choices.'
        - '<b>Ethical</b>: Mobilizon is developed on a non-profit basis, so there
          will be no profiling nor attention-grabbing mechanism. On the contrary,
          it is designed to give maximum power to the people who use it.'
        - '<b>Human</b>: Mobilizon is not developed by an out-of-this-world StartUp,
          but by a group of friends who strive to <a href="https://framasoft.org">change
          the world, one byte at a time</a>. So certainly we go slower, but we stay
          attentive and in touch. Moreover, Mobilizon was designed by asking activists
          about their digital needs.'
    federated:
      q: Why is it better for it to be a federated tool?
      a:
        p:
        - 'Let’s imagine that my university creates its instance <em>MobilizCollege</em>
          on the one hand, and my climate movement creates its instance <em>EcoMobilized</em>
          on the other hand: do I have to create an account on each site, just to
          keep up with the events?'
        - 'No: in our opinion, this would be a major obstacle to use. That’s why we
          want Mobilizon to be federated: each instance (each event publication website)
          powered by Mobilizon will then be able to choose to exchange with other
          instances, to display more events than just its own, and to promote interactions.'
        - Federation using the most widespread standard (named <a href="https://en.wikipedia.org/wiki/ActivityPub">ActivityPub</a>),
          will also allow, in the long run, to build bridges with <a href="https://joinmastodon.org">Mastodon</a>
          (the free and federated alternative to Twitter), <a href="https://joinpeertube.org">PeerTube</a>
          (free and federate alternative to YouTube), and many other alternative tools.
    install:
      q: How do I install Mobilizon on a server?
      a:
        p:
        - 'For the moment, it is not possible: the code is still under development
          and there are no installation facilitations nor guidelines yet.'
        - You will need to wait for the release of the beta version (autumn 2019)
          and version 1 (first half of 2020) to get your installation facilitated
          by complete documentation, and even packaging.
        - However, if you want to follow the work in progress development, <a href="https://framagit.org/framasoft/mobilizon">the
          code is there</a>.
    develop:
      q: How to participate in the Mobilizon code?
      a:
        p:
        - First of all, you will need knowledge of Git and Elixir. If you don’t know
          them, it means that, for the time being, the project is not in a position
          to receive your contributions in code.
        - But if you do, just go to <a target="_blank" rel="noopener noreferrer" href="https://framagit.org/framasoft/mobilizon/">the
          software repository</a>, and write an issue, or fork it to start proposing
          your own contributions.
        - Of course, it’s always better to come and talk with our developers beforehand,
          by using <a target="_blank" rel="noopener noreferrer" href="https://riot.im/app/#/room/#Mobilizon:matrix.org">our
          Matrix</a> chan.
    contribute:
      q: How do I participate in Mobilizon if I can't code?
      a:
        p:
        - The easiest way is to come and talk to us, on <a target="_blank" rel="noopener
          noreferrer" href="https://framacolibri.org/c/mobilizon">the Mobilizon-dedicated
          category</a> in our contributions forum.
        - 'Remember that we are not a multi-national tech giant, nor even a big start-up,
          but a not-for-profit of less than 40 members (with other projects
          in parallel): don’t be offended if we need time to answer you!'
        - You probably have some great ideas to bring to the project, and thank you
          if you take the time to propose them. However, we know that we can deliver
          what we promised to do for the beta version and V1, but we also know that
          we cannot add to this already busy program.
        - Thus, it is to be expected that your ideas and proposals will not be processed
          until development after version 1, which we (Framasoft) hope to be 
          able to carry out (but this will depend on the success of this
          crowdfunding!).
    cost:
      q: How much will it cost me to use Mobilizon?
      a:
        p:
        - The goal of financing the production of this software is that a large number
          of organizations can then offer its free use, as is done for most <a href="https://joinmastodon.org">Mastodon</a>
          (the free and federated alternative to Twitter) and <a href="https://joinpeertube.org">PeerTube</a>
          (free and federated alternative to YouTube) instances.
        - What is certain is that the Mobilizon software will be distributed free
          of charge and that no one will have to pay us anything to install it on their
          server (and we will even try to make their life easier).
        - Our hope is that enthusiasm for this service will give us the means to
          open an instance, public and free, perhaps with the release of V1.
    timeline-q:
      q: When can I use Mobilizon?
      a:
        p:
        - Today, the code is still under development, and does not allow for simple
          and straightforward use.
        - 'In the fall of 2019, we plan to release a beta version, of which the features
          will depend on the amount raised during crowdfunding. This version,
          which is equivalent to a first draft, will allow an unsecured use: the documentation
          will be a work in progress, and there will probably be improvements
          to be made.'
        - If we reach the 2<sup>nd</sup> level of crowdfunding, we will open a demonstration
          instance to the public, where everyone can click and experiment, but where
          data will be deleted on a regular basis.
        - Mobilizon will be easily usable by everyone in the 
          first half of 2020, when version 1 is released.
  crowdfunding:
    title: Crowdfunding questions
    third-platform:
      q: Why not use a platform like GoFundMe or KickStarter?
      a:
        p:
        - Framasoft is an <i lang="fr">association</i> domiciled in France, which
          means we can’t use many platforms requiring a tax domiciliation in North
          America. In addition, the French platforms (which <a href="https://fr.ulule.com/etherpad-framapad/">we
          have already used</a>, on <a href="https://www.kisskissbankbank.com/fr/projects/peertube-a-free-and-federated-video-platform">multiple
          occasions</a>) do not offer a tool that exactly complies with the crowdfunding
          we wanted to do this time.
        - So we decided to save on the 5% commission that these intermediaries earn
          on average, and to create our own fundraising website. We already had the
          banking and accounting tools to collect donations, so we just had to create
          the web interface.
        - The advantage of having a homemade tool is that your data does not pass
          through one more intermediary. As for all the <a href="https://degooglisons-internet.org">ethical
          services</a> that we have been offering for years, here too, <a href="https://framasoft.org/fr/cgu/">your
          data does not interest us</a>, because we sincerely value your privacy.
    trust:
      q: How do I know if I can trust you?
      a:
        p:
        - This is a question that must be asked, because everyone has their own levels
          of trust, which depend on many things. Here, we’ll give you some information,
          then it’s up to you to make up your own mind.
        - 'Framasoft is a not-for-profit association (under the French 1901 act):
          Our aim is not to make money, just to earn the means to pursue our actions
          and achieve financial balance. So we have no interest in exploiting your
          data, profiling you, etc.'
        - 'Framasoft’s business model is generosity: we are funded from individuals’
          donations, and have been for years. Our accounts are audited by an independent
          auditor, whose <a href="https://framasoft.org/fr/association/">reports
          are published here</a>, in French (2018 report is currently being processed).'
        - Our actions are known and recognized in the French-speaking free software
          community. From our <a href="https://framalibre.org ">free resources directory</a>
          to our <a href="https://framabook.org ">free licensed book publishing house</a>
          via the <a href="https://degooglison-internet.org ">34 alternative services</a>
          that we host to Degooglize the Internet, we have demonstrated our
          commitment to strong ethics.
        - With more than 15 years of existence, Framasoft’s reputation is established
          (search us!). We have already undertaken crowdfunding campaigns to 
          finance the development of <a href="https://fr.ulule.com/etherpad-framapad/">MyPads</a>
          and <a href="https://www.kisskissbankbank.com/fr/projects/peertube-a-free-and-federated-video-platform">PeerTube</a>,
          which we have developed <a href="https://joinpeertube.org/en/news/">beyond
          what was promised</a> (and funded) during these campaigns.
    noreward:
      q: Why don't you offer any reward?
      a:
        p1:
        - Because the only real counterpart here is the creation of a digital <em>common</em>.
          Indeed, Mobilizon is <a href="https://framagit.org/framasoft/mobilizon/blob/master/LICENSE">under
          a free license</a>, which means that it belongs to everyone, without anyone
          being able to appropriate it exclusively.
        - The system of rewards of the classic crowdfunding costs a lot of time, money
          and energy. But we are willing to bet that enough people will be willing to fund a credible
          alternative to Facebook events, just so that one day everyone can enjoy
          them.
        - Finally, we do not want a reward system that discriminates between contributors on
          the basis of their income, because everyone has different means, and everyone
          can participate, according to their means, in the birth of a digital common.
        - 'Thus, any person who contributes to financing Mobilizon will be entitled
          (if they wish) to:'
        ul:
        - get their (nick)name in Mobilizon’s readme file and on Joinmobilizon.org’s
          Hall of Fame
        - receive in their mailbox the Mobilizon newsletter
    receipt:
      q: Will I get a receipt for my donation?
      a:
        p:
        - Yes, provided that you tick the appropriate box on the donation form and
          provide us with a mailing address, which is required by law for us to issue
          a donation receipt.
        - Your receipt will be automatically generated in March 2020, and sent to
          the email address you have provided.
        - If you need a receipt before this date, or if you change your email address,
          you can <a href="https://contact.framasoft.org">contact us here</a>.
    taxes:
      q: Will my donation be tax deductible?
      a:
        p:
        - Yes, for French taxpayers.
        - Framasoft being an "association of general interest" (under French classification),
          any donation can entitle you to <a href="https://framablog.org/2018/11/22/impots-et-dons-a-framasoft-le-prelevement-a-la-source-en-2019/">a
          66% reduction (FR)</a> on your income taxes.
        - Thus, a "UX maintenance" donation at €130 would, after deduction, cost French
          taxpayers €44.20.
        - If you do not pay income tax in France, please check with your country’s
          tax authorities.
news:
  title: What’s new on Mobilizon?
  subtitle: Discover the latest improvements of the tool
  last-post: Latest articles
  blocs:
    19-05-14:
      title: The levels of Mobilizon crowdfunding
      text:
        p0:
        - We believe in the need to create user-friendly software that allows people
          to to organize their events away from Facebook or Meetup (for example).
        - We believe in it so much that we have invested time, work and money to imagine
          its design.
        - Our association being financed only by donations, we organize this crowdfunding
          to ask you how far you want us to go in the development of Mobilizon.
        h41: Bearing 1 - Free & basic version
        p1:
        - <img src="@:data.baseurl/img/en/01-palier-mobilizon-minimaliste.png" class="img-fluid"
          alt="">
        - This sum will allow us to recover our expenses.
        - We will thus reimburse the sums advanced to design and promote software,
          working time on the development of its functionalities as well as the work
          of the designers who facilitated its design.
        - The code will be returned to the community in a state that can be taken
          back by anyone.
        h42: Level 2 - Federated version
        p2:
        - <img src="@:data.baseurl/img/en/02-palier-mobilizon-federe.png" class="img-fluid"
          alt="">
        - 'We will be able to implement an essential protocol to make Mobilizon a
          success: the ActivityPub federation. Thanks to the federation, any community
          will be able to install Mobilizon on its own servers, and connect together
          each of these installations.'
        - This makes it possible to multiply the number of entry points and decentralize
          the data, and to connect to the fediverse which already includes Mastodon
          (alternative to Twitter) and PeerTube (an alternative to YouTube).
        - We will also open a demo instance of this beta version, so that everyone
          can go and see for themselves what the software will look like.
        h43: Bearing 3 - Ideal version
        p3:
        - <img src="@:data.baseurl/img/en/03-palier-mobilizon-ideal.png" class="img-fluid"
          alt="">
        - You will give us the means to make the Mobilizon of our dreams come true!
        - Thus, Mobilizon will allow the creation of groups, with messaging tools
          and exchange between members. Mobilizon will also make it possible to organize,
          display and to manage the external tools that the group already uses to
          write documents together or choose the date of the next meeting, for example.
        - Finally, each person with an account will be able to create several identities,
          in order, for example, not to display the same identity on its sporting
          events, family reunions and activist actions.
        h44: And beyond - To version 1
        p4:
        - <img src="@:data.baseurl/img/en/04-palier-mobilizon-version-1.png" class="img-fluid"
          alt="">
        - 'We have done our calculations: with €50,000, we will have the means to
          to make the best software we can make. If we receive more money, we will
          not do more, nor faster, but we can go further.'
        - In the fall of 2019, we will release a beta version. From then on, rich
          in feedback and comments we will receive, will open the way to a first version
          completed, a V1, which we want for the first half of 2020.
        - But we still need a lot of work until then. In our desires, it there is
          the possibility to set up a mapping server to facilitate the localization
          of events, to create a webapp for smartphones, and many other things…
media:
  title: Media area
